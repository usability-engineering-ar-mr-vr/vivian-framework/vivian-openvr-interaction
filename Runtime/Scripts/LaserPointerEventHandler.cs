﻿using de.ugoe.cs.vivian.core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointerEventHandler : MonoBehaviour
{
    // the prototype in the scene while interacting
    private VirtualPrototype[] Prototypes = null;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SteamVR_TrackedController>().TriggerClicked += new ClickedEventHandler(OnTriggerClicked);
        this.GetComponent<SteamVR_TrackedController>().TriggerUnclicked += new ClickedEventHandler(OnTriggerUnClicked);
    }

    // Update is called once per frame
    void OnTriggerClicked(object sender, ClickedEventArgs args)
    {
        this.Prototypes = FindObjectsOfType<VirtualPrototype>();

        if (this.Prototypes != null)
        {
            Pose pose = this.GetPose();

            foreach (VirtualPrototype prototype in this.Prototypes)
            {
                prototype.TriggerInteractionStarts(pose, (int) this.GetComponent<SteamVR_TrackedController>().controllerIndex);
            }
        }
    }

    // Update is called once per frame
    void OnTriggerUnClicked(object sender, ClickedEventArgs args)
    {
        if (this.Prototypes != null)
        {
            Pose pose = this.GetPose();

            foreach (VirtualPrototype prototype in this.Prototypes)
            {
                prototype.TriggerInteractionEnds(pose, (int)this.GetComponent<SteamVR_TrackedController>().controllerIndex);
            }
        }
    }

    void Update()
    {
        if ((this.GetComponent<SteamVR_TrackedController>().triggerPressed) &&
            (this.Prototypes != null))
        {
            Pose pose = this.GetPose();

            foreach (VirtualPrototype prototype in this.Prototypes)
            {
                prototype.TriggerInteractionContinues(pose, (int)this.GetComponent<SteamVR_TrackedController>().controllerIndex);
            }
        }
    }

    private Pose GetPose()
    {
        return new Pose(this.gameObject.transform.position, this.gameObject.transform.rotation);
    }
}
