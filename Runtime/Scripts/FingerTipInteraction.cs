﻿using de.ugoe.cs.vivian.core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace de.ugoe.cs.vivian.openvrinteraction
{
    public class FingerTipInteraction : MonoBehaviour
    {
        // counter for finger ids
        public static int fingerId = 0;

        // the effective finger id to use for this object instance
        private int EffectiveFingerId = fingerId++;

        // the prototype in the scene while interacting
        private VirtualPrototype[] Prototypes = null;

        // stores whether this finger is currently interacting
        private bool Interacting = false;

        // stores whether this hand is currently opened
        private bool HandStatusOpened = false;

        // called when colliding with another object
        void OnTriggerEnter(Collider collider)
        {
            if (!this.HandStatusOpened && !this.Interacting)
            {
                this.Prototypes = FindObjectsOfType<VirtualPrototype>();

                if (this.Prototypes != null)
                {
                    Pose pose = this.GetPose();

                    foreach (VirtualPrototype prototype in this.Prototypes)
                    {
                        prototype.TriggerInteractionStarts(pose, this.EffectiveFingerId);
                    }

                    this.Interacting = true;
                }
            }
        }

        // called every frame --> updates the interaction ray if necessary
        void Update()
        {
            if (this.Interacting && (this.Prototypes != null))
            {
                Pose pose = this.GetPose();

                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionContinues(pose, this.EffectiveFingerId);
                }
            }
        }

        // called when leaving the collider of another object
        void OnTriggerExit(Collider collider)
        {
            if (this.Interacting && (this.Prototypes != null))
            {
                Pose pose = this.GetPose();

                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionEnds(pose, this.EffectiveFingerId);
                }

                this.Interacting = false;
            }
        }

        /**
         * Called if the user opens his/her hand
         */
        public void HandOpened()
        {
            if (this.Interacting && (this.Prototypes != null))
            {
                Pose pose = this.GetPose();

                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionEnds(pose, this.EffectiveFingerId);
                }

                this.Interacting = false;
                this.HandStatusOpened = true;
            }
        }

        /**
         * Called if the user closes his/her hand
         */
        public void HandClosed()
        {
            this.HandStatusOpened = false;
        }

        /**
         * used to determine the ray running through the tip of the index finger and pointing forward.
         * Ensures the ray to start somewhere inside the finger to better detect collisions with planes
         * that are touched
         */
        private Pose GetPose()
        {
            Vector3 direction = this.transform.rotation * Vector3.forward;
            Vector3 origin = this.transform.position - (direction.normalized * 0.05f);
            Pose pose = new Pose(origin, this.transform.rotation);
            Debug.DrawLine(origin, origin + direction, Color.green);
            return pose;
        }
    }
}